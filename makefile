#ledger invoice/statement utility
#version 20200530

#use one shell invocation per recipe
.ONESHELL :

#cause shell invocation to fail if one command in the recipe fail
.SHELLFLAGS += -e

#default value if not supplied by user
export from := $(shell date --date="$(date +%Y-%m-15) -1 month" '+%Y-%m-01')
export to := $(shell date --iso-8601)

.PHONY : invoice statement clean pricedb

invoice :
	@echo "generating invoice $(invoice) ..."
	if [[ ! -v invoice ]]; then echo 'no invoice number. use make invoice=<number>'; exit 1; fi
	mkdir -pv invoices
	#recover invoice data
	data_1=$$(ledger -f journal.ledger --price-db .pricedb --pedantic --register-format "\"%(account)\" %(scrub(amount))\n" reg code ^$(invoice)$$)
	#recover date, code and payee
	data_2=$$(ledger -f journal.ledger --date-format "%Y-%m-%d" --pedantic --register-format "%(date) %(code) \"%(payee)\"\n%/" reg code ^$(invoice)$$)
	#recover prices
	data_3=$$(ledger -f journal.ledger --price-db .pricedb --date-format "%Y-%m-%d" --pedantic prices)
	#recover invoice date
	date=$$(echo -e "$${data_2}" | awk '{print $$1; exit}')
	if [[ -z $${date} ]]; then echo 'no transaction found for invoice: $(invoice).'; exit 1; fi
	#filter up to date market prices
	echo -e "$${data_3}" | awk '$$1 <= date' date="$${date}" | awk '{
	if ($$2 in a) #if commodity is already in array
	{
	    if ($$1 >= a[$$2][1]) #if commodity record is equal or newer than saved one
	    {
		#update commodity date and price
		a[$$2][1]=$$1; a[$$2][2]=$$3
	    }
	}
	else
	{
	    #commodity is not in array, create a new entry
	    a[$$2][1]=$$1; a[$$2][2]=$$3
	}}
	END {
	for (com in a)
	{
	    #display up to date commodity prices
	    print com, a[com][2]
	}}' > /run/.pricedb.tmp
	#recover client account name
	client=$$(echo -e "$${data_1}" | awk 'BEGIN {FPAT="\"[^\"]*\""} /clients:/ {NF=1; FS=":"; $$0=gensub(/"/, "", "g", $$0); print $$NF; exit}')
	if [[ -z $${client} ]]; then echo 'no client account found for invoice: $(invoice). client account must be a subaccount of "clients" account'; exit 1; fi
	#recover invoice description
	invoice_description=$$(echo -e "$${data_2}" | awk 'BEGIN {FPAT="\"[^\"]*\""} {print gensub(/"/, "", "g", $$1); exit}')
	#recover data/myself line number
	myself_lines=$$(awk '{} END {print NR}' data/myself)
	#recover data/<client> line number
	client_lines=$$(awk '{} END {print NR}' data/$${client// /-})
	#setup postscript file
	echo '%!PS' > invoices/$(invoice).ps
	cat data/Hack-Regular.t42 >> invoices/$(invoice).ps
	echo "%define some useful procedures
	%move point to x,y and paint glyphs from (text)
	%usage: (text) x y ms -
	/ms {moveto show} def
	%mm to point
	%usage: mm mmtopt pt
	/mmtopt {0.35306 div} def
	/fontheight 12 def
	/margin 5 mmtopt def
	/xlimit 612 def
	/ylimit 792 def
	<< /PageSize [612 792] >> setpagedevice
	/Hack-Regular fontheight selectfont
	/invoice ($(invoice)) def
	/date ($${date}) def
	/myself_lines $${myself_lines} def
	/client_lines $${client_lines} def
	/parsep 3 def
	/char_width (x) stringwidth pop def
	/invoice_description ($${invoice_description}) def
	/header_y ylimit margin sub myself_lines parsep add client_lines add parsep add fontheight mul sub def
	/code_x margin def
	/code_width 14 def
	/description_x code_x code_width char_width mul add def
	/description_width 36 def
	/quantity_x description_x description_width char_width mul add def
	/quantity_width 8 def
	/price_x quantity_x quantity_width char_width mul add def
	/price_width 12 def
	/total_x price_x price_width char_width mul add def
	/total_width 12 def
	%calculate y position based on line number
	%usage: line yp ypos
	/yp {1 add fontheight mul header_y exch sub} def
	/subtotal_y margin 3 fontheight mul add def
	/tps_y margin 2 fontheight mul add def
	/tvq_y margin fontheight add def
	/total_y margin def" >> invoices/$(invoice).ps
	#print sender information
	awk '{print "(" $$0 ") margin ylimit margin sub " NR " fontheight mul sub ms"}' data/myself >> invoices/$(invoice).ps
	#print invoice number and date
	echo 'invoice xlimit margin sub invoice stringwidth pop sub ylimit margin sub fontheight sub ms
	(invoice: ) xlimit margin sub invoice stringwidth pop sub (invoice: ) stringwidth pop sub ylimit margin sub fontheight sub ms
	date xlimit margin sub date stringwidth pop sub ylimit margin sub fontheight 2 mul sub ms
	(date: ) xlimit margin sub date stringwidth pop sub (date: ) stringwidth pop sub ylimit margin sub fontheight 2 mul sub ms
	(Client:) margin ylimit margin sub myself_lines parsep add fontheight mul sub ms' >> invoices/$(invoice).ps
	#print client information
	awk '{print "(" $$0 ") margin ylimit margin sub myself_lines parsep add " NR " add fontheight mul sub ms"}' data/$${client// /-} >> invoices/$(invoice).ps
	#print invoice description
	echo '(Description:) xlimit margin sub (Description:) stringwidth pop sub ylimit margin sub myself_lines parsep add fontheight mul sub ms
	invoice_description xlimit margin sub invoice_description stringwidth pop sub ylimit margin sub myself_lines parsep add 1 add fontheight mul sub ms' >> invoices/$(invoice).ps
	#print items header
	echo '(code) code_x header_y ms
	(description) description_x header_y ms
	(qty) quantity_x header_y ms
	(price) price_x header_y ms
	(total) total_x header_y ms' >> invoices/$(invoice).ps
	#print invoice data
	echo -e "$${data_1}" | awk '#get commodity description in another file
	#com: commodity name
	function getdesc(com)
	{
	    sr=$$0 #save record
	    FPAT="\"[^\"]*\""
	    while ((getline < ".commoditydb") > 0)
	    {
		if ($$0 ~ "^commodity +" com "$$")
		{
		    getline < ".commoditydb"
		    close(".commoditydb")
		    desc=$$1 #save description
		    FS=FS #restore field separator as before FPAT
		    $$0=sr #restore record
		    return gensub(/"/, "", "g", desc)
		}
	    }
	    FS=FS
	    $$0=sr
	}
	#get up to date market price for commodity
	#com: commodity name
	function getprice(com)
	{
	    sr=$$0 #save record
	    while ((getline < "/run/.pricedb.tmp") > 0)
	    {
		if ($$1 ~ com)
		{
		    close("/run/.pricedb.tmp")
		    price=$$2 #save up to date price
		    $$0=sr #restore record
		    return price
		}
	    }
	    $$0=sr
	}
	BEGIN {CONVFMT="%4.2f"}
	/clients:/ {
	$$0=gensub(/"[^"]*"/, "", 1, $$0) #remove account field from record
	if ($$2 !~ /^\$$$$/)
	{
	    print "(" gensub(/"/, "", "g", $$2) ") code_x " NR " yp ms"
	    print "(" getdesc($$2) ") description_x " NR " yp ms"
	    print "(" $$1 ") quantity_x " NR " yp ms"
	    price=getprice($$2)
	    print "(" price ") price_x " NR " yp ms"
	    item_total=$$1 * price
	    print "(" item_total ") total_x " NR " yp ms"
	    subtotal+=item_total #calculate invoice subtotal
	}
	}
	END {
	print "(subtotal) price_x subtotal_y ms"
	print "(" subtotal ") total_x subtotal_y ms"
	print "/subtotal " subtotal " def" #save subtotal to postscript variable
	}' >> invoices/$(invoice).ps
	#print invoice total
	echo -e "$${data_1}" | awk '#compute absolute value of <v>
	function abs(v)
	{return ((v < 0.0) ? -v : v)}
	BEGIN {tps=0; tvq=0} #set default values for taxes
	{
	#get account and remove it from record
	FPAT="\"[^\"]*\""; $$0=$$0; account=$$1; FS="[ ]"; $$0=gensub(/"[^"]*"/, "", 1, $$0)
	}
	account ~ /:.*tps/ {tps+=abs($$2)} #add up taxes
	account ~ /:.*tvq/ {tvq+=abs($$2)}
	END {
	print "(tps) price_x tps_y ms"
	print "(" tps ") total_x tps_y ms"
	print "(tvq) price_x tvq_y ms"
	print "(" tvq ") total_x tvq_y ms"
	print "(total) price_x total_y ms"
	print "subtotal " tps, tvq " add add price_width string cvs total_x total_y ms"
	}' >> invoices/$(invoice).ps
	#print footer
	awk 'NR==FNR {count++} NR!=FNR {print "(" $$0 ") margin " (count - FNR) " fontheight mul margin add ms"}' data/invoice-footer data/invoice-footer >> invoices/$(invoice).ps
	ps2pdf invoices/$(invoice).ps invoices/$(invoice).pdf
	rm /run/.pricedb.tmp invoices/$(invoice).ps
	echo "generated invoice $(invoice) successfully"

statement :
	@echo "generating account statement from $(from) to $(to) for $(account) ..."
	if [[ ! -v account ]]; then echo 'no account name. use make account=<name>'; exit 1; fi
	mkdir -pv statements
	#convert account name to account data filename
	acc_file=$${account/ /-}
	#merge postings with same date, code and payee
	cmd='BEGIN {
	FS="[ ]" #set field separator
	}
	{
	a[NR][1]=$$1; a[NR][2]=$$2; sr=$$0 #save date, code and complete record
	sFS=FS; FPAT="\"[^\"]*\""; $$0=$$0 #save field separator, change field pattern and reparse record
	a[NR][3]=$$1; FS=sFS; $$0=gensub(/"[^"]*"/, "", 1, sr) #save payee, restore field separator and reparse record without payee
	a[NR][4]=$$4; a[NR][5]=$$6 #save amount and total
	}
	END {
	for (line in a)
	{
	    for (oline in a)
	    {
		#if line is different from base one and date, code and payee are the same
		if (oline != line && a[oline][1] == a[line][1] && a[oline][2] == a[line][2] && a[oline][3] == a[line][3])
		{
		    #merge posting into base one
		    a[line][4] += a[oline][4]
		    a[line][5] += a[oline][4]
		    a[oline][1] = "skip" #mark line to skip printing it
		}
	    }
	    if (a[line][1] != "skip") print a[line][1], a[line][2], a[line][3], a[line][4], a[line][5]
	}
	}'
	data_1=$$(ledger -f journal.ledger --price-db .pricedb --date-format "%Y-%m-%d" --historical --pedantic --register-format "%(format_date(date)) %(code) \"%(payee)\" %(scrub(display_amount)) %(scrub(display_total))\n" reg "$(account)" | awk "$${cmd}")
	#recover data/myself line number
	myself_lines=$$(awk '{} END {print NR}' data/myself)
	#recover data/<client> line number
	client_lines=$$(awk '{} END {print NR}' data/$${acc_file})
	#setup postscript file
	echo '%!PS' > statements/$${acc_file}.ps
	cat data/Hack-Regular.t42 >> statements/$${acc_file}.ps
	echo "%define some useful procedures
	%move point to x,y and paint glyphs from (text)
	%usage: (text) x y ms -
	/ms {moveto show} def
	%mm to point
	%usage: mm mmtopt pt
	/mmtopt {0.35306 div} def
	/fontheight 12 def
	/margin 5 mmtopt def
	/xlimit 612 def
	/ylimit 792 def
	<< /PageSize [612 792] >> setpagedevice
	/Hack-Regular fontheight selectfont
	/date ($(to)) def
	/myself_lines $${myself_lines} def
	/client_lines $${client_lines} def
	/parsep 3 def
	/char_width (x) stringwidth pop def
	/header_y ylimit margin sub myself_lines parsep add client_lines add parsep add fontheight mul sub def
	/date_x margin def
	/date_width 12 def
	/code_x date_x date_width char_width mul add def
	/code_width 8 def
	/description_x code_x code_width char_width mul add def
	/description_width 38 def
	/amount_x description_x description_width char_width mul add def
	/amount_width 12 def
	/balance_x amount_x amount_width char_width mul add def
	/balance_width 12 def
	%calculate y position based on line number
	%usage: line yp ypos
	/yp {1 add fontheight mul header_y exch sub} def" >> statements/$${acc_file}.ps
	#print sender information
	awk '{print "(" $$0 ") margin ylimit margin sub " NR " fontheight mul sub ms"}' data/myself >> statements/$${acc_file}.ps
	#print account statement date
	echo '(account statement) xlimit margin sub (account statement) stringwidth pop sub ylimit margin sub fontheight sub ms
	date xlimit margin sub date stringwidth pop sub ylimit margin sub fontheight 2 mul sub ms
	(date: ) xlimit margin sub date stringwidth pop sub (date: ) stringwidth pop sub ylimit margin sub fontheight 2 mul sub ms
	(Client:) margin ylimit margin sub myself_lines parsep add fontheight mul sub ms' >> statements/$${acc_file}.ps
	#print client information
	awk '{print "(" $$0 ") margin ylimit margin sub myself_lines parsep add " NR " add fontheight mul sub ms"}' data/$${acc_file} >> statements/$${acc_file}.ps
	#print header
	echo '(date) date_x header_y ms
	(code) code_x header_y ms
	(description) description_x header_y ms
	(amount) amount_x header_y ms
	(balance) balance_x header_y ms' >> statements/$${acc_file}.ps
	#print statement data
	echo -e "$${data_1}" | awk 'BEGIN {
	FS="[ ]"; #set field separator
	}
	{
	print "(" $$1 ") date_x " NR " yp ms"
	print "(" $$2 ") code_x " NR " yp ms"
	sr=$$0; sFS=FS; FPAT="\"[^\"]*\""; $$0=$$0 #save record, field separator, change field pattern and reparse record
	print "(" gensub(/"/, "", "g" , $$1) ") description_x " NR " yp ms"
	FS=sFS; $$0=gensub(/"[^"]*"/, "", 1, sr) #restore field separator and reparse record without payee
	print "(" $$4 ") amount_x " NR " yp ms"
	print "(" $$5 ") balance_x " NR " yp ms"
	}' >> statements/$${acc_file}.ps
	#print balance due
	ledger -f journal.ledger --price-db .pricedb --date-format "%Y-%m-%d" --historical --pedantic --balance-format "%(scrub(display_total))\n" bal "$(account)" | awk '{print "(" $$1 ") balance_x margin ms"; print "(balance due) amount_x margin ms"}' >> statements/$${acc_file}.ps
	#print footer
	awk 'NR==FNR {count++} NR!=FNR {print "(" $$0 ") margin " (count - FNR) " fontheight mul margin add ms"}' data/statement-footer data/statement-footer >> statements/$${acc_file}.ps
	ps2pdf statements/$${acc_file}.ps statements/$${acc_file}.pdf
	rm statements/$${acc_file}.ps
	echo "generated account statement from $(from) to $(to) for $(account) successfully"

clean :
	@rm -rv invoices statements

pricedb :
	@if [[ ! -v correction ]]; then echo 'no correction factor. use make pricedb correction=<factor>'; exit 1; fi
	awk 'BEGIN {
	old_corr=0
	CONVFMT="%4.2f"
	}
	/^;correction/ {
	old_corr=$$2 #get old correction factor
	$$2=correction #print new correction factor
	}
	($$0 ~ /^P/ && old_corr != 0) {
	$$4=$$4 / old_corr * correction
	}
	{print}' correction="$${correction}" .pricedb > /run/.pricedb.tmp
	mv /run/.pricedb.tmp .pricedb
